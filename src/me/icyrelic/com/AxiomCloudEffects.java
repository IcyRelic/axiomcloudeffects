package me.icyrelic.com;


import me.icyrelic.com.Commands.clouds;
import me.icyrelic.com.Console.ConsoleMessage;

import java.util.HashMap;
import java.util.UUID;

import me.icyrelic.com.Listeners.CloudEffects;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class AxiomCloudEffects extends JavaPlugin {
	
	
	public HashMap<UUID, String> clouds_effects = new HashMap<UUID, String>();
	public boolean debug = false;
	public ConsoleMessage cm = new ConsoleMessage();
	
	public void onEnable(){
		
		if(getAxiomAPI() == null){
			cm.sendMessage("AxiomAPI Not found! Shutting Down...");
			
			getServer().getPluginManager().disablePlugin(this);
		}else{
			getCommand("clouds").setExecutor(new clouds(this));
			Bukkit.getServer().getPluginManager().registerEvents(new CloudEffects(this), this);
			loadConfiguration();
			cm.sendMessage("Enabled");
			
			
			
			
			
			
		}
		
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	
	
	private AxiomAPI getAxiomAPI() {
	    Plugin pl = getServer().getPluginManager().getPlugin("AxiomAPI");
	 
	    if (pl == null || !(pl instanceof AxiomAPI)) {
	        return null;
	    }
	 
	    return (AxiomAPI) pl;
	}
}
