package me.icyrelic.com.Commands;



import java.util.ArrayList;
import java.util.UUID;

import me.icyrelic.com.AxiomAPI;
import me.icyrelic.com.AxiomCloudEffects;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class clouds implements CommandExecutor {
	
	String noPerm = (ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission!");
	String noPermE = (ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use that cloud effect!");
	AxiomCloudEffects plugin;
	public clouds(AxiomCloudEffects instance) {

		plugin = instance;

		}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("clouds")) {
			
			if(sender instanceof Player){
				Player p = (Player) sender;
				UUID uid = p.getUniqueId();
				
				if(p.hasPermission("AxiomCloudEffects.clouds")){
					
					if(args.length >= 1){
						if(args[0].equalsIgnoreCase("help")){
							
							p.sendMessage(ChatColor.GOLD + "Cloud Effects Help");
							p.sendMessage(ChatColor.GRAY + "/clouds clear - Clear your cloud");
							p.sendMessage(ChatColor.GRAY + "/clouds reload - Reload the config");
							p.sendMessage(ChatColor.GRAY + "/clouds list - List of cloud effects you can use");
							p.sendMessage(ChatColor.GRAY + "/clouds <effect> - Equip a cloud effect");
							p.sendMessage(ChatColor.GRAY + "/clouds preview <id>:<data> [<particles>] - Preview an effect");
							p.sendMessage(ChatColor.GRAY + "/clouds create <effect> - Make a new cloud effect");
							p.sendMessage(ChatColor.GRAY + "/clouds remove <effect> - Remove a cloud effect");
							
						}else if(args[0].equalsIgnoreCase("reload")){
							if(p.hasPermission("cloud.clear")){
								plugin.reloadConfig();
								p.sendMessage(ChatColor.DARK_GREEN + "Config Reloaded");
							}else{
								p.sendMessage(noPerm);
							}
						}else if(args[0].equalsIgnoreCase("create")){
							if(p.hasPermission("cloud.create")){
								
								if(!p.hasPermission("cloud.create.bypass")){
									int customs = 0;
									
									if(!getAxiomAPI().api.getStored(p.getName()+"_custom_cloud").equalsIgnoreCase("Invalid ID")){
										customs = Integer.parseInt(getAxiomAPI().api.getStored(p.getName()+"_custom_cloud"));
									}
									
									if(customs >= 1){
										if(args.length == 3){
											plugin.getConfig().set("clouds."+args[1]+".makeup", args[2]);
											plugin.saveConfig();
											
											int value = Integer.parseInt(getAxiomAPI().api.getStored(p.getName()+"_custom_cloud"));
											int finalint = value-1;
											getAxiomAPI().api.removeStored(p.getName()+"_custom_cloud");
											getAxiomAPI().api.store("int", p.getName()+"_custom_cloud", finalint+"");
											plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "pex user "+p.getName()+" add cloud.effects."+args[1]);
											p.sendMessage(ChatColor.DARK_GREEN + "The effect "+ChatColor.GREEN+ChatColor.BOLD+args[1]+ChatColor.DARK_GREEN+" has been created");
										}else{
											p.sendMessage(ChatColor.RED+"USAGE: "+ChatColor.DARK_RED+"/clouds create <effect> <makeup>");
										}
										
										
										
									}else{
										p.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have any custom cloud tokens");
									}
									
								}else{
									if(args.length == 3){
										plugin.getConfig().set("clouds."+args[1]+".makeup", args[2]);
										plugin.saveConfig();
										p.sendMessage(ChatColor.DARK_GREEN + "The effect "+ChatColor.GREEN+ChatColor.BOLD+args[1]+ChatColor.DARK_GREEN+" has been created");
									}else{
										p.sendMessage(ChatColor.RED+"USAGE: "+ChatColor.DARK_RED+"/clouds create <effect> <makeup>");
									}
								}
								
								
								
								
								
								
							}else{
								p.sendMessage(noPerm);
							}
						}else if(args[0].equalsIgnoreCase("remove")){
							if(p.hasPermission("cloud.remove")){
								
								if(args.length == 2){
									plugin.getConfig().set("clouds."+args[1]+".makeup", null);
									plugin.saveConfig();
									p.sendMessage(ChatColor.DARK_GREEN + "The effect "+ChatColor.GREEN+ChatColor.BOLD+args[1]+ChatColor.DARK_GREEN+" has been removed");
								}else{
									p.sendMessage(ChatColor.RED+"USAGE: "+ChatColor.DARK_RED+"/clouds remove <effect>");
								}
								
								
								
							}else{
								p.sendMessage(noPerm);
							}
						}else if(args[0].equalsIgnoreCase("clear")){
							
							if(p.hasPermission("cloud.clear")){

								if(plugin.clouds_effects.containsKey(uid)){
									plugin.clouds_effects.remove(uid);
								}
								
								p.sendMessage(ChatColor.DARK_GREEN + "Your cloud effects have been removed");
							}else{
								p.sendMessage(noPerm);
							}
							
						}else if(args[0].equalsIgnoreCase("preview")){
							
							if(p.hasPermission("cloud.preview")){

								if(args.length >= 2){
									
									int times = 50;
									int done = 0;
									
									
									if(plugin.clouds_effects.containsKey(uid)){
										plugin.clouds_effects.remove(uid);
									}
									
									Location center = p.getPlayer().getLocation();
									String makeup = args[1];
									
									p.sendMessage(ChatColor.DARK_GREEN + "You are now previewing the " + ChatColor.GREEN + ChatColor.BOLD + makeup + ChatColor.DARK_GREEN + " cloud effect");
									
									while (done < times){
										if(makeup.contains(",")){
											String[] split = makeup.split(",");
											
											for(String s: split){
												int x = 50 / split.length;
												int y = 0;
												
												
												String[] iddata = s.split(":");
												int id = Integer.parseInt(iddata[0]);
												int data = Integer.parseInt(iddata[1]);
												
												while (y < 50){
													
													
													p.spigot().playEffect(center, Effect.TILE_BREAK, id, data, 0, 0, 0, 30, 50, 30);
													
													
													
													//ParticleEffect.displayBlockCrack(center, block, (byte) value, 0, 0, 0, x);
													y = y+x;
												}
												
											}
										}else{
											String[] split = makeup.split(":");
											int id = Integer.parseInt(split[0]);
											int data = Integer.parseInt(split[1]);
											
										    p.spigot().playEffect(center, Effect.TILE_BREAK, id, data, 0, 0, 0, 30, 50, 30);
										}
										done++;
									}
									
									
								}else{
									p.sendMessage(ChatColor.RED + "USAGE: " + ChatColor.DARK_RED + "/clouds preview <id:data>");
								}
								
							}else{
								p.sendMessage(noPerm);
							}
							
						}else if(args[0].equalsIgnoreCase("list")){
							
							String clouds = "";
							boolean first = true;
							ArrayList<String> perms = new ArrayList<String>();
							for(String s: plugin.getConfig().getConfigurationSection("clouds").getKeys(false)){
								if(plugin.getConfig().getString("clouds."+s+".makeup") != null){
									perms.add("cloud.effects."+s);
								}
							}
							
							perms.add("cloud.effects.heart");
							
							for(String perm: perms){
								
								
								if(p.hasPermission(perm)){
									if(!first){
										clouds = clouds+", ";
									}
									clouds = clouds+perm.replace("cloud.effects.", "");
									first = false;
								}
								
							}
							
							if(clouds.equals("")){
								clouds = "None";
							}
							
							p.sendMessage(ChatColor.GOLD + "Cloud Effects:");
							p.sendMessage(ChatColor.GREEN + clouds);
							
						}else if(args[0].equalsIgnoreCase("debug")){
							
							if(p.hasPermission("cloud.dubug")){

								plugin.debug = !plugin.debug;
								
								p.sendMessage(ChatColor.GOLD + "DEBUG_MODE: " + plugin.debug);
							}else{
								p.sendMessage(noPerm);
							}
							
						}else{
							String effect = "";
							String makeup = "";
							boolean match = false;
							for(String s: plugin.getConfig().getConfigurationSection("clouds").getKeys(false)){
								if(args[0].equalsIgnoreCase(s) && plugin.getConfig().getString("clouds."+s+".makeup") != null){
									effect = s;
									makeup = plugin.getConfig().getString("clouds."+effect+".makeup");
									match = true;
								}
							}
							
							String[] otherEffects = {"heart"};
							
							for(String s : otherEffects){
								
								if(args[0].equalsIgnoreCase(s)){
									effect = s;
									makeup = s;
									match = true;
								}
							}
							
							if(!match){
								p.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "Invalid Cloud Effect. Type /cloud list to view cloud effects");
							}else{
								if(p.hasPermission("cloud.effects."+effect)){
									if(plugin.clouds_effects.containsKey(uid)){
										plugin.clouds_effects.remove(uid);
									}
									plugin.clouds_effects.put(uid, makeup);
									p.sendMessage(ChatColor.DARK_GREEN + "You now have the " + ChatColor.GREEN + ChatColor.BOLD + effect + ChatColor.DARK_GREEN + " cloud effect equipped");
								}else{
									p.sendMessage(noPermE);
								}
							}
							
							
						}
						
					}else{
						p.sendMessage(ChatColor.GOLD + "AxiomCloudEffects");
					}
					
				}else{
					p.sendMessage(noPerm);
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "ERROR: "+ChatColor.DARK_RED +"This command must be run by a player");
			}
			
		}
		return true;
	}
	
	
	private AxiomAPI getAxiomAPI() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("AxiomAPI");
	 
	    if (pl == null || !(pl instanceof AxiomAPI)) {
	        return null;
	    }
	 
	    return (AxiomAPI) pl;
	}
}


	
	
