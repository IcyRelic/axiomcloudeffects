package me.icyrelic.com.Listeners;

import java.util.UUID;

import me.icyrelic.com.AxiomCloudEffects;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class CloudEffects implements Listener {
	
	AxiomCloudEffects plugin;
	public CloudEffects(AxiomCloudEffects instance) {

		plugin = instance;

		}
	
	int timesDebug = 0;

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		UUID uid = p.getUniqueId();
		if(plugin.clouds_effects.containsKey(uid)){
			
			Location center = e.getPlayer().getLocation();
			String makeup = plugin.clouds_effects.get(uid);
			
			
			switch(makeup){
				
				case "heart":
					playHeart(p,center);
					break;
				default:
					playBlockCrack(p,makeup,center);
					break;
					
			}
			
			
		}
		
	}
	
	
	private void playHeart(Player p, Location center){
		
		p.spigot().playEffect(center, Effect.HEART, 0, 0, 0, 0, 0, 30, 50, 30);
		
	}
	
	
	
	private void playBlockCrack(Player p, String makeup, Location center){
		
		if(makeup.contains(",")){
			String[] split = makeup.split(",");
			
			for(String s: split){
				int x = 50 / split.length;
				int y = 0;
				
				
				String[] iddata = s.split(":");
				int id = Integer.parseInt(iddata[0]);
				int data = Integer.parseInt(iddata[1]);
				
				while (y < 50){
					
					
					p.spigot().playEffect(center, Effect.TILE_BREAK, id, data, 0, 0, 0, 30, 50, 30);
					
					
					
					//ParticleEffect.displayBlockCrack(center, block, (byte) value, 0, 0, 0, x);
					y = y+x;
				}
				
			}
		}else{
			String[] split = makeup.split(":");
			int id = Integer.parseInt(split[0]);
			int data = Integer.parseInt(split[1]);
			
		   p.spigot().playEffect(center, Effect.TILE_BREAK, id, data, 0, 0, 0, 30, 50, 30);
		}
		
	}
}
