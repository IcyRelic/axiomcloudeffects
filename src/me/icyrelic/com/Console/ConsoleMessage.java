package me.icyrelic.com.Console;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;

public class ConsoleMessage {
	
	private String prefix = (ChatColor.WHITE+"[" + ChatColor.GOLD + "AxiomCloudEffects" + ChatColor.WHITE + "] ");
	
	public void sendMessage(String msg){
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		console.sendMessage(prefix + msg);
	}

	public void sendMessage(Material mat) {
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		console.sendMessage(prefix + mat);
	}

	public void sendMessage(int data) {
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		console.sendMessage(prefix + data);
		
	}

	public void sendMessage(String[] split) {
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		console.sendMessage(prefix + split);
		
	}
	
	
	

}
